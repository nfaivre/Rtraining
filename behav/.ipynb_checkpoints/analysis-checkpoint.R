## R tutorial for behavioral data

# house keeping -----------------------------------------------------------
rm(list=ls(all=TRUE)) # clear workspace
library(tidyverse)  # load a useful library (install.packages('tidyverse') before)

rootdir = dirname(rstudioapi::getActiveDocumentContext()$path) # get directory in which the script is saved
setwd(rootdir) # change directory

fileslist = dir(paste(rootdir,'data',sep='/'), glob2rx('*.csv'),full.names = T)
a = lapply(fileslist, read.csv) # read behavioral file

b = c()
for (i in 1:length(a)) {
  names(a[[i]]) =c('resp','rt','volt','suj')
  b = rbind(b,a[[i]])
}
a = as.data.frame(b)
## check data fomat
str(a)
a$suj = as.factor(a$suj)

# descriptive statistics --------------------------------------------------
indivdata = aggregate(cbind(resp,rt) ~ volt + suj,a,mean) 
indivdata$length = aggregate(cbind(resp) ~ volt + suj,a,length)$resp



# plotting ----------------------------------------------------------------
# psychometric curve
ggplot(indivdata,aes(x=volt,y=resp)) + geom_point() + 
  geom_smooth(method="glm",method.args = list(family = "quasibinomial"),alpha=.1) + facet_wrap(~suj)

# reaction times ~ volt (+ quadratic component as an example)
ggplot(indivdata,aes(x=volt,y=rt)) + geom_point() + stat_smooth(method=lm, formula=y~poly(x, 2)) +
  facet_wrap(~suj)

# boxplots
ggplot(a,aes(y=rt,x=factor(resp),color=factor(resp)))  + geom_boxplot(outlier.shape = NA) + geom_jitter(width=.2,alpha=.2) + facet_wrap(~suj)

# density/histogram of reaction times
ggplot(a,aes(x=rt,y= ..density..,fill=factor(resp))) + geom_density(alpha=.2,position=position_dodge())
ggplot(a,aes(x=rt,y= ..count..,fill=factor(resp))) + geom_histogram(alpha=.2,position=position_dodge()) #+ facet_wrap(~resp)


# for psychometric curves with more than two parameters: http://dlinares.org/quickpsy.html 

library(quickpsy) 

fit <- quickpsy(a,volt,resp,random = .(suj),fun=logistic_fun, 
                prob = .5,guess=T,lapses=T,parini = list(c(0,3),c(0,2),c(0,.3),c(0, .3)),bootstrap='none') # parini: thresh,slope,guess,lapse

# plotcurves(fit)
curves=fit$curves
pfit=ggplot(curves,aes(x=x,y=y)) + geom_line() + facet_wrap(~suj)
meanresp=aggregate(resp~volt+suj,a,mean)
meanresp$length=aggregate(resp~volt+suj,a,length)$resp/10; 
pfit + geom_point(data=meanresp,aes(x=volt,y=resp,group=suj),alpha=.2,size=4)

## plot the parameters
vals=as.data.frame(fit$par)
# vals = vals %>%  spread(parn,par)
vals$parn = ifelse(vals$parn=='p1','Threshold',
                   ifelse(vals$parn=='p2','Slope',
                          ifelse(vals$parn=='p3','Low asymptote',
                                 ifelse(vals$parn=='p4','High asymptote','??'))))
ggplot(vals,aes(x="Parameter",y=par)) + geom_boxplot() + facet_wrap(~parn)

