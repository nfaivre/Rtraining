% read mat files and creat common csv file
behavfiles = dir(['.' filesep 'data' filesep '*.mat']);
for b = 1:length(behavfiles)
    load(['.' filesep 'data' filesep behavfiles(b).name])
    for t = 1:length(Info.T)
        behav(t,1)=Info.T(t).Answer;
        behav(t,2)=Info.T(t).RT;
        behav(t,3)=Info.T(t).Voltage;
        behav(t,4)=b;
    end
    csvwrite(['.' filesep 'data' filesep behavfiles(b).name '.csv'],behav)
end