---
title: "Fit and check mixed-effects logistic regressions"
output:
  html_document:
    theme: united
    toc: yes
    toc_float: yes
  pdf_document:
    toc: yes
---

Analyze behavioral data from perithr_faces2
See: https://gitlab.com/nfaivre/perithr_faces2.git

# R setup
```{r setup, warning=F, message=F, echo=F}
## load libraries
libs2load = c('tidyverse','sjPlot','emmeans','here','lme4','lmerTest','DHARMa')
lapply(libs2load, library, character.only = TRUE)

# change default color
theme_set(theme_bw())
scale_colour_discrete <-
  function(...) {
    scale_colour_brewer(..., palette = "Set2")
  }
scale_fill_discrete <-
  function(...) {
    scale_fill_brewer(..., palette = "Set2")
  }
```

# Load and curate data
```{r load, warning=F, message=F}
a = read_tsv(here('data','toydata.tsv'))
a$sub = as.factor(a$sub) # recode subject ID as factor
a$dur_face = ifelse(a$dur_face==5,'short','long') # recode duration as factor 
```

# Explo plots
```{r plot, warning=F, message=F}

# explo plots
a %>% group_by(sub,dur_face) %>% summarise(n=length(resp)) %>% 
  ggplot(aes(dur_face,n)) + geom_dotplot(binaxis='y',stackdir='center')

a %>% ggplot(aes(relative_int,resp,color=dur_face,fill=dur_face)) + 
  stat_summary(fun=mean,geom='point') + 
  geom_smooth(method='glm', method.args = list(family = "binomial"),alpha=0.1) +
  facet_wrap(~sub)
```

# fit glmer
```{r glmer fit}
# corresponding models
control=glmerControl(optimizer="bobyqa",optCtrl=list(maxfun=10000))
mtest = lmer(resp ~ relative_int * dur_face  + (1|sub),data=a) # wrong!
m1 = glmer(resp ~ relative_int * dur_face  + (1|sub),family=binomial,data=a)
m2 = glmer(resp ~ relative_int * dur_face  + (relative_int|sub),family=binomial,data=a)
m3 = glmer(resp ~ relative_int * dur_face  + (dur_face|sub),family=binomial,data=a)
m4 = glmer(resp ~ relative_int * dur_face  + (relative_int + dur_face|sub),family=binomial,data=a)
m22 = glmer(resp ~  dur_face  + (1|sub),family=binomial,data=a)

# model comparison
anova(m1,m2)
anova(m2,m4)

# summary model
summary(m2)
```

# Model diagnostics
```{r diag}
# plot model diagnostics using DHARMa
resid = simulateResiduals(m1, plot = F)
plot(resid)
plotResiduals(resid, a$relative_int, quantreg = F,asFactor=T)
```

# Plot model predictions
```{r pred}
# plot model fixed effects
plot_model(m2,type= 'est') + scale_color_brewer(palette='Set2') + scale_fill_brewer(palette='Set2')

# plot model predictions
avg = a %>% group_by(sub,relative_int,dur_face) %>% summarise(resp = mean(resp))
plot_model(m2,type='pred',terms=c('relative_int [0.8:1.4 by=.01]','dur_face')) + 
  geom_point(data=avg, inherit.aes = F,aes(relative_int,resp,color=dur_face),
             position=position_jitterdodge(jitter.width=.025,jitter.height=0,dodge.width = .1),alpha=.3) +
  stat_summary(data=avg, inherit.aes = F,aes(relative_int,resp,color=dur_face),
               fun.data=mean_cl_normal,geom='pointrange') + 
  labs(x='Relative Intensity',y='% Seen response',title='') +
  scale_x_continuous(breaks=c(0,0.8,1,1.2,1.4))+
  scale_color_brewer(palette='Set2') + scale_fill_brewer(palette='Set2')
```

# model contrasts  
```{r contrasts}
# contrasts
emmeans(m2, pairwise ~ dur_face | relative_int,cov.keep='relative_int')
test(emtrends(m2, ~ dur_face,var='relative_int'))
```

# bonus: equivalent of m2 with bayesian framework
```{r bayesian}
library(brms)

# define priors
priors <- c(set_prior("normal(3,2)", class = "b", coef= "relative_int"),
            set_prior("normal(0,2)", class = "b", coef= "dur_faceshort"),
            set_prior("normal(0,2)", class = "b", coef= "relative_int:dur_faceshort"))

m2_b = brm(resp ~ relative_int * dur_face  + (relative_int + dur_face|sub), data = a, prior=priors,family = brms::bernoulli(),
           sample_prior = T, chains = 4,iter = 5000,warmup = 2500, cores= 4,
           file_refit='on_change', file=here('models','glm_cor')) 

summary(m2_b)
plot(m2_b)

# compute bayes factors
h <- c("intensity" = "relative_int > 0")
(hyp=hypothesis(m2_b, h))
plot(hyp)

h <- c("intensity*duration" = "relative_int:dur_faceshort  = 0")
(hyp=hypothesis(m2_b, h))
plot(hyp)

# average fit
avg = a %>% group_by(sub,relative_int,dur_face) %>% summarise(resp = mean(resp))

plot_model(m2_b,type = "pred",terms=c('relative_int [0.8:1.4 by=.01]','dur_face')) +
  geom_point(data=avg, inherit.aes = F,aes(relative_int,resp,color=dur_face),
             position=position_jitterdodge(jitter.width=.025,jitter.height=0,dodge.width = .1),alpha=.3) +
  stat_summary(data=avg, inherit.aes = F,aes(relative_int,resp,color=dur_face),
               fun.data=mean_cl_normal,geom='pointrange') + 
  labs(x='Relative Intensity',y='% Seen response',title='') +
  scale_x_continuous(breaks=c(0,0.8,1,1.2,1.4))+
  scale_color_brewer(palette='Set2') + scale_fill_brewer(palette='Set2')
```

