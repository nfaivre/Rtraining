## R tutorial for EEG data
## N. Faivre March 2018

# house keeping ----------------------------------------------------------- 
rm(list=ls(all=TRUE))
library(lme4);library(lmerTest) # mixed model stuff
library(Hmisc)
library(tidyverse); theme_set(theme_bw(base_size = 10)); 
library(afex)
library(multidplyr) # install with devtools::install_github("tidyverse/multidplyr")
library(zoo)
library(eegUtils) # install with devtools::install_github("craddm/eegUtils")
library(caret)

rootdir = dirname(rstudioapi::getActiveDocumentContext()$path) # get directory in which the script is saved
setwd(rootdir)

source('./functions/myload_set.R')
ci <- function (x) { 1.96*sd(x)/sqrt(length(x))}

cluster=new_cluster(4)
cluster %>% cluster_library(c('dplyr','afex','purrr','caret','zoo')) # add relevant libraries to different clusters for later usage

dopreproc  = 1  # redo preprocessing or just load preprocessed data
locking    = 'resplocked'; # resplocked or stimlocked?
cond       = '' # maybe specify an experimental condition later


eeglist<-dir(paste(rootdir,'data',sep='/'), '*.set') # list EEG files

# Preprocessing -----------------------------------------------------------
if (dopreproc == 1) {
  a=list()
  # read preprocessed files from EEGLAB iteratively and append to a
  for (f in 1:length(eeglist)) {
    message(paste('loading',eeglist[f],sep=' '))
    EEG = myload_set(paste(rootdir,'data',eeglist[f],sep='/'))
    tmp = cbind(EEG[[1]],EEG[[3]]) %>% select(-sample)
    tmp = tmp %>% gather(chan,amp,names(tmp)[1:63])
    
    events=as.data.frame(EEG[[6]])
    tmp=full_join(tmp,events, by ='epoch')
    tmp$suj=substr(f,1,3)
    a[[f]] = tmp %>% filter(time> -.5,time<.5)# create a column for subject
  }
  a = bind_rows(a) 
  ## reencode time variable in ms
  a$time = a$time * 1000
  
  ## exclude bad guys if any
  badguys = c('')
  a = a[!is.element(a$suj,badguys),]
  
  ## baseline correction 
  a <- left_join(a, a %>%
                   filter(time <= -400) %>%
                   group_by(epoch,suj) %>% 
                   summarise(baseline = mean(amp))) 
  a$amp_bl <- a$amp - a$baseline
  a = a %>% select(-baseline)
  
  ## tag outliers based on IQR
  a <- left_join(a, a %>%
                   group_by(time,suj,chan) %>%
                   summarise(limsup = median(amp_bl)+2*IQR(amp_bl),liminf = median(amp_bl)-2*IQR(amp_bl))) 
  
  a$rm = ifelse(a$amp_bl<a$liminf | a$amp_bl>a$limsup,1,0)
  mean(a$rm) # trial exclusion
  ggplot(a[a$chan=='C3',],aes(x=time,y=amp_bl,color=factor(rm))) + geom_point() + facet_wrap(~suj)
  # a = a %>% filter(rm==0) %>% select(-rm,-limsup,-liminf) # uncomment if needed
  
  ## compute whether conf is > or < than median
  a <- left_join(a, a %>%
                   group_by(suj) %>% 
                   summarise(medconf = median(conf))) 
  a$medconf = ifelse(a$conf>a$medconf,'high','low')
  
  ## bin confidence with quantiles
  a = a %>% group_by(suj) %>% mutate(quantconf = ntile(conf, 3)) 
  a$quantconf = as.factor(a$quantconf)
  
  save(a,file=paste('data_clean.RData',sep='')) 
  
  
  ## read chan locs
  elec = read_csv('./functions/chanlocs62.csv')
  elec$radianTheta <- pi/180*elec$theta
  elec <- elec %>%
    mutate(x = .$radius*sin(.$radianTheta),
           y = .$radius*cos(.$radianTheta))
  elec=data.frame(electrode=elec$chan,x=elec$x,y=elec$y)
  save(elec,file='elec.RData')
  
}
if (dopreproc==0) {
  load(file='data_clean.RData')
  load('elec.RData')
}

# PLOT BASICS -------------------------------------------------------------
# ## plot raw EEG data
a %>%
  select(amp,chan,time,epoch,suj) %>%
  group_by(chan,time,suj) %>%
  summarise(amp = mean(amp,na.rm=T)) %>% 
  ggplot(aes(x = time, y = amp,group=chan)) +
  geom_path(alpha=0.1)  + facet_wrap(~suj)
# ggsave('./plots/rawdat.pdf',device='pdf')

## plot avg ERP for cor/incor on a given channel
#  example with parallel computing (partition & collect using multidplyr)
a %>%
  filter(is.element(chan,c('CPz','Cz','C4','C3'))) %>%
  group_by(chan,time,cor, suj) %>% partition(cluster) %>% 
  summarise(amp = mean(amp,na.rm=T)) %>% collect() %>% 
  ggplot(aes(x = time, y = amp,group=cor,color=factor(cor),fill=factor(cor))) +
  stat_summary(fun.data = mean_cl_normal, geom = "ribbon", alpha = 0.1,color=F)+
  stat_summary(fun.y = mean, geom = "line") + facet_wrap(~chan)
# ggsave('./plots/corincor_avg.pdf',device='pdf')

## plot individual ERP for cor/incor on a given channel
a %>%
  filter(is.element(chan,c('CPz'))) %>%
  group_by(chan,time,cor, suj) %>%
  summarise(amp = mean(amp,na.rm=T)) %>%
  ggplot(aes(x = time, y = amp,group=cor,color=factor(cor)),fill=factor(cor)) +
  stat_summary(fun.y = mean, geom = "line") + facet_wrap(~suj)
ggsave('./plots/corincor_indiv.pdf',device='pdf')


## plot a topo cor - incor on filtered time
topo=a %>%
  filter(time>450 & time<=550) %>%
  group_by(chan,cor, suj) %>%
  summarise(amp_bl = mean(amp_bl,na.rm=T)) %>%
  group_by(chan,cor) %>%
  summarise(amp_bl = mean(amp_bl,na.rm=T)) %>%
  spread(cor,amp_bl) %>%
  mutate(amplitude = `1` - `0`)
topoplot(cbind(data.frame(amplitude=topo$amplitude),elec))

## plot an ERP heatmap
range01 <- function(x){(x-min(x))/(max(x)-min(x))}
a %>% filter(chan=='C3' ,time <100, suj=="1") %>% group_by(time) %>% arrange(time,rt) %>% mutate(trial2=1:n()) %>% 
  group_by(trial2) %>% mutate(amp=rollmean(amp,20, fill=NA)) %>%
  ggplot(aes(x=time,y=trial2,fill=amp)) + geom_raster() + scale_fill_distiller(palette='RdBu',na.value = "white") + 
  geom_point(aes(x=range01(rt),y=trial2)) + geom_vline(xintercept=0,linetype='dashed')

# RUN LMER IN PARALLEL ----------------------------------------------------
chans=unique(a$chan)
times = unique(a$time)
allres=list()
for (c in 1:length(times)) {
  print(paste('fitting timepoint = ',times[c],'ms',sep=''))
  allres[[c]] = a %>%
    filter (time == times[c]) %>%
    select(amp,conf,cor,suj,chan,rt) %>%
    group_by(chan) %>% nest() %>% partition(cluster) %>% 
    # nest(data=c(cor,conf,suj,rt,amp)) %>%
    # multidplyr::partition(chan) %>%
    mutate(lme_out = map(data, ~mixed(amp~conf+rt  + (1|suj), data = .,method='S',progress=F))) %>% # impossible to add random slope for conf
    mutate(perf = map(lme_out,"anova_table")) %>%
    mutate(F = map(perf,'F')) %>%
    mutate(p = map(perf,'Pr(>F)')) %>% select(-perf,-data,-lme_out) %>% as.data.frame() %>%
    collect() %>% mutate(time =times[c])
  
}

save(allres,file='allres.RData')
allres=bind_rows(allres)
## tidy up
Fs=as_tibble(matrix(unlist(allres$F),ncol=2,byrow = T),.name_repair="minimal"); names(Fs) = c('conf','rt')
ps=as_tibble(matrix(unlist(allres$p),ncol=2,byrow = T),.name_repair="minimal"); names(ps) = c('pconf','prt')
widestats = allres %>% select(-F,-p) %>% as_tibble() 
widestats = cbind(widestats,Fs,ps)
longstats = widestats  %>%   pivot_longer(names_to = 'effect', cols =  c(conf,rt)) %>% select(chan,time,effect,F=value)
tmp = widestats  %>%   pivot_longer(names_to = 'effect', cols =  c(pconf,prt)) 
longstats$p = tmp$value

## plot results
ggplot(longstats,aes(x=time,y=log(p),color=effect)) + geom_path() +
  geom_hline(yintercept = c(log(0.05),log(0.01),log(0.001)),linetype = 'dashed',alpha=0.5)+
  geom_vline(xintercept = 0,alpha=.2)+
  facet_wrap(~chan)
# ggsave('./plots/lmer_conf.pdf',device='pdf')

## plot summed F value
longstats %>%
  group_by(time,effect) %>%
  summarise(F=sum(F))%>%
  ggplot(aes(x=time,y=F,color=effect)) + geom_path() + xlab("temps") + ylab("F-value")
# ggsave('./plots/lmer_Fconf.pdf',device='pdf')

## corresponding topography
longstats = longstats %>%  group_by(chan,effect) %>% mutate(p.fdr=p.adjust(p,'fdr'))
topotimes=unique(longstats$time[which(longstats$p.fdr<0.01)])

p=list()
for (t in 1:length(topotimes)) {
  amptmp=longstats$F[longstats$time==topotimes[t] & longstats$effect=="conf"]
  p[[t]] = topoplot(cbind(data.frame(amplitude=amptmp),elec),thick=.2) + ggtitle(topotimes[t])
}
do.call(grid.arrange,p)


# MACHINE LEARNING --------------------------------------------------------
fitControl <- trainControl(method = "repeatedcv",number = 10,repeats=5,returnData=F,returnResamp='final',savePredictions='final',preProcOptions = list())
cluster_copy(cluster, 'fitControl')

classify_data= a %>% 
  dplyr::select(chan,conf,amp,time,epoch,suj) %>%
  arrange(suj,epoch,chan,time) %>%
  group_by(suj,epoch,chan) %>% partition(cluster) %>% 
  mutate(amp=rollmean(amp,25, fill=NA)) %>% # averaging over sliding window
  collect() %>% 
  # mutate(amp = cumsum(abs(amp)) / seq_along(amp)) %>% 
  filter(!is.na(amp)) %>% 
  spread(chan,amp) %>% ungroup()  %>%
  dplyr::select(-epoch) %>% group_by(time,suj) %>% nest()
 


m1 = classify_data %>%
  partition(cluster) %>%
  mutate(fit = map(data, ~train(conf ~ ., data = .x,  preProcess = c('center','scale'),method = "glmnet",trControl = fitControl,na.action=na.omit))) %>% 
  collect()

save(m1,file='m1_glmnet_conf.RData')
# load('m1_glmnet_conf.RData')

## classification
# m1res <- mutate(m1, perf = map(fit,"resample") %>%
#                        map("Kappa") %>%
#                        map_dbl(~mean(.x,na.rm=T)))

# ## regression
m1res <- mutate(m1, perf = map(fit,'results') %>% # resample
                  map("Rsquared") %>%
                  map_dbl(~mean(.x,na.rm=T))) %>%
  select(-data,-fit)

## plot results
m1res %>%
  ggplot(aes(x=time, y=perf))+
  stat_summary(fun.data = mean_cl_normal, geom = "ribbon", alpha = 0.2) +
  stat_summary(fun.y = mean, geom = "line")
# ggsave('./plots/glmnet_conf_avg.pdf',device='pdf')

m1res %>%
  ggplot(aes(x=time, y=perf,color=suj,group=suj))+ geom_path()
# ggsave('./plots/glmnet_conf_indiv.pdf',device='pdf')

